/* Copyright(C) 2022. Huawei Technologies Co.,Ltd. All rights reserved.
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

// Package v1
package v1

import (
	"encoding/json"
	"errors"
	"fmt"
	"testing"

	"github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/assert"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	_ "hccl-controller/pkg/testtool"
)

// TestUnmarshalToRankTable test UnmarshalToRankTable
func TestUnmarshalToRankTable(t *testing.T) {
	convey.Convey("TestRankTableV1 UnmarshalToRankTable", t, func() {
		r := &RankTableStatus{}
		convey.Convey("UnmarshalToRankTable() should return err == nil &&"+
			" r.status == ConfigmapInitializing when Normal", func() {
			err := r.UnmarshalToRankTable(`{"status":"initializing"}`)
			convey.So(err, convey.ShouldEqual, nil)
			convey.So(r.Status, convey.ShouldEqual, ConfigmapInitializing)
		})
		convey.Convey("UnmarshalToRankTable should return err != nil when "+
			"jobString == "+`"status": "initializing"`, func() {
			err := r.UnmarshalToRankTable(`"status": "initializing"`)
			convey.So(err, convey.ShouldNotEqual, nil)
			convey.So(r.Status, convey.ShouldEqual, "")
		})
		convey.Convey("UnmarshalToRankTable should return err != nil when jobString == "+
			`{"status":"xxxxx"} `, func() {
			err := r.UnmarshalToRankTable(`{"status":"xxxxx"}`)
			convey.So(err, convey.ShouldNotEqual, nil)
		})
	})
}

func TestCheckDeviceInfo(t *testing.T) {
	convey.Convey("TestRankTableV1 TestCheckDeviceInfo", t, func() {
		instance := Instance{
			Devices:  []Device{{DeviceID: "2", DeviceIP: "0.0.0.0"}, {DeviceID: "3", DeviceIP: "0.0.0.0"}},
			PodName:  "podname",
			ServerID: "0.0.0.0",
		}
		convey.Convey("CheckDeviceInfo() should return true when Normal", func() {
			isOk := CheckDeviceInfo(&instance)
			convey.So(isOk, convey.ShouldEqual, true)
		})
		convey.Convey("CheckDeviceInfo() should return false when ServerID  is not an IP address", func() {
			instance.ServerID = "127.0.0.1s"
			isOk := CheckDeviceInfo(&instance)
			convey.So(isOk, convey.ShouldEqual, false)
		})
		convey.Convey("CheckDeviceInfo() should return false when DeviceID  is less than zero", func() {
			instance.Devices[0].DeviceIP = "-1"
			isOk := CheckDeviceInfo(&instance)
			convey.So(isOk, convey.ShouldEqual, false)
		})
		convey.Convey("CheckDeviceInfo() should return false when Devices  is empty", func() {
			instance.Devices = []Device{}
			isOk := CheckDeviceInfo(&instance)
			convey.So(isOk, convey.ShouldEqual, false)
		})
		convey.Convey("CheckDeviceInfo() should return false when DeviceIP  is not an IP address", func() {
			instance.Devices[0].DeviceIP = "127w.0.0.1s"
			isOk := CheckDeviceInfo(&instance)
			convey.So(isOk, convey.ShouldEqual, false)
		})

	})
}

// TestCachePodInfo test CachePodInfo
func TestCachePodInfo(t *testing.T) {
	fmt.Println("TestRankTableV1 TestCachePodInfo")
	group := &Group{GroupName: "t1", DeviceCount: "1", InstanceCount: "1", InstanceList: []*Instance(nil)}
	groupList := append([]*Group(nil), group)
	fake := &RankTable{RankTableStatus: RankTableStatus{Status: ConfigmapInitializing}, GroupCount: "1",
		GroupList: groupList}
	po := &v1.Pod{ObjectMeta: metav1.ObjectMeta{Name: "test1"}}
	rank := "1"
	const (
		podString     = `{"pod_name":"0","server_id":"0.0.0.0","devices":[{"device_id":"0","device_ip":"0.0.0.0"}]}`
		RankNumExpect = "1"
	)
	var instance Instance
	if err := json.Unmarshal([]byte(podString), &instance); err != nil {
		instance = Instance{}
	}

	fmt.Println("CachePodInfo() should return err == nil when Normal ")
	err := fake.CachePodInfo(po, instance, rank)
	assert.Equal(t, nil, err)
	assert.Equal(t, RankNumExpect, rank)
	deviceIP := fake.GroupList[0].InstanceList[0].Devices[0].DeviceIP
	assert.Equal(t, "0.0.0.0", deviceIP)

	fmt.Println("CachePodInfo() should return err != nil when podName == group.Instance.PodName")
	fake.CachePodInfo(po, instance, rank)
	po2 := &v1.Pod{ObjectMeta: metav1.ObjectMeta{Name: "0"}}
	err = fake.CachePodInfo(po2, instance, rank)
	assert.Equal(t, nil, err)
	assert.Equal(t, RankNumExpect, rank)

	fmt.Println("CachePodInfo() should return err != nil when deviceInfo is wrong")

	if err = json.Unmarshal([]byte(`{"pod_name":"0","server_id":}`), &instance); err != nil {
		instance = Instance{}
	}
	err = fake.CachePodInfo(po, instance, rank)
	assert.NotEqual(t, nil, err)
	assert.Equal(t, RankNumExpect, rank)

	fmt.Println("CachePodInfo() should return err != nil when len(GroupCount) <1 ")
	fake = &RankTable{RankTableStatus: RankTableStatus{Status: ConfigmapInitializing},
		GroupCount: "1", GroupList: nil}
	if err = json.Unmarshal([]byte(""), &instance); err != nil {
		instance = Instance{}
	}
	err = fake.CachePodInfo(nil, instance, rank)
	assert.NotEqual(t, nil, err)
}

// TestRemovePodInfo test RemovePodInfo
func TestRemovePodInfo(t *testing.T) {

	convey.Convey("TestRankTableV1 RemovePodInfo", t, func() {
		group := &Group{GroupName: "t1", DeviceCount: "1", InstanceCount: "1", InstanceList: []*Instance(nil)}
		groupList := append([]*Group(nil), group)
		fake := &RankTable{RankTableStatus: RankTableStatus{Status: ConfigmapInitializing},
			GroupCount: "1", GroupList: groupList}
		po := &v1.Pod{ObjectMeta: metav1.ObjectMeta{Name: "test1", UID: "dadsadada"}}
		rank := "1"
		const podString = `{"pod_name":"test1","server_id":"0.0.0.0","devices":[{"device_id":"0",
"device_ip":"127.0.0.1"}]}`
		var instance Instance
		if err := json.Unmarshal([]byte(podString), &instance); err != nil {
			instance = Instance{}
		}
		convey.Convey("RemovePodInfo() should return err == nil when Normal", func() {
			expectErr := errors.New("no data of pod /dadsadada can be removed")
			fake.CachePodInfo(po, instance, rank)
			err := fake.RemovePodInfo("", po.UID)
			convey.So(err, convey.ShouldResemble, expectErr)
			convey.So(len(fake.GroupList[0].InstanceList), convey.ShouldEqual, 1)
		})

		convey.Convey("RemovePodInfo() should return err != nil when podName !contain GroupList ", func() {
			fake.CachePodInfo(po, instance, rank)
			err := fake.RemovePodInfo("", "0")
			convey.So(err, convey.ShouldNotEqual, nil)
			convey.So(len(fake.GroupList[0].InstanceList), convey.ShouldEqual, 1)
		})

	})
}
