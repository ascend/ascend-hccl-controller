/* Copyright(C) 2020-2023. Huawei Technologies Co.,Ltd. All rights reserved.
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

// Package common for common value
package common

const (
	// BitSize32 BitSize32
	BitSize32 = 32
	// Decimal Decimal
	Decimal = 10
	// MaxJobParallelism max job Parallelism
	MaxJobParallelism = 32
	// MaxPodParallelism max pod Parallelism
	MaxPodParallelism = 32
	// InformerInterval informer interval time.
	InformerInterval = 30

	// Index0 index 0
	Index0 = 0
	// Index1 index 1
	Index1 = 1
	// Index2 index 2
	Index2 = 2
	// Index3 index 3
	Index3 = 3

	// A800MaxChipNum the max num of Ascend910(B) in Atlas 800 server
	A800MaxChipNum = 16

	// PodHasBeenCachedLogPattern log pattern of pod has been cached
	PodHasBeenCachedLogPattern = "ANOMALY: pod %s/%s is already cached"
)
